<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="estilo.css">
    <title>Algoritmo de Luhn</title>


</head>
<body>
    
    <section class="container-fluid row justify-content-center">
        <section class="contenedor">

            <form action="" name="form" id="formulario" method="POST">

                <h3 class="center">Obtener No. de verificación</h3><br>

                <label>No. de tarjeta</label>
                <input autocomplete="false" type="text" id="tarjeta" name="numero" placeholder="Introduce tu número de cuenta"><p>

            
                <button type="submit" onclick="respuesta()">Validar</button>
                
            </form>

        </section>
    </section>

    <script src="app.js"></script>

</body>
</html>