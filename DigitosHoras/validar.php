<?php
    $hora1 = $_POST['hora1'];
    $hora2 = $_POST['hora2'];

    if(!isset($hora1) || !isset($hora2)){
        echo("<script type='text/javascript'>alert('Rellenar los campos');") ;
        echo("location.replace('index.php');");
        echo("</script>");
    }
    
    echo("<script type='text/javascript'>alert('Hay ".tiemposInteresantes($hora1, $hora2)." tiempos interesantes entre ".$hora1." y ".$hora2." ');") ;
    echo("location.replace('index.php');");
    echo("</script>");
    
    function tiemposInteresantes($t1, $t2) {

        $veces = 0;

        $hoy = date('Y-m-d');
        $time1 = strtotime("$hoy $t1");//Convertimos la hora a segundos. Los segundo se empiezan a contar desde 1970
        $time2 = strtotime("$hoy $t2");


        for($tiempo = $time1; $tiempo <= $time2; $tiempo++) {
                
            $veces += esInteresante($tiempo); 
            //echo $veces;
        }

        return $veces;
        
    }

    function esInteresante($time) {

        $hms = date('His', $time); //Convierte los segundos, en una hora normal His(HH:MM:SS)

        //str_split convertimos un string en un arreglo
        $digitos = array_unique(str_split($hms));//buscamos valores duplicados

        return count($digitos)<=2; //contamos los arreglos que tienen 2 o menos digitos
    }

?>