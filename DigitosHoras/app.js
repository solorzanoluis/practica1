let m = "2022-01-07T22:22:21.417-06:00";
let m2 = "2022-01-07T22:22:23.417-06:00";

let ms = Date.parse(m); // a milisegundos
let ms2 = Date.parse(m2);

//Funcion para convertir un numero a arreglo
function ConvertirArreglo(numero) {

    if (numero < 0) {
        return null;
    }

    return [...
        `${numero}`
    ].map(c => parseInt(c)); //.map(c=>parseInt(c) mapeamos la cadena y por cada c caracter convertimos a entero a c
}

//Convierto mis resultados en arreglo
var result1 = ConvertirArreglo(ms);

var result2 = ConvertirArreglo(ms2);

//Función para separar digitos
function extraer10(datos, n = 1) {

    if (!Array.isArray(datos[0])) {
        return datos.slice(0, n); //Si no se le pasa elsegundo argumento, debe mostrar [1]
    }
    return datos[0].slice(0, n);
}



var resultado1 = extraer10(result1, 10); //Extraemos los primeros 10 digitos
var valorFinal1 = resultado1.join(''); //Unimos los valores del arreglo
console.log(valorFinal1);

var resultado1 = extraer10(result2, 10);
var valorFinal2 = resultado1.join('');
console.log(valorFinal2);


alert('Hay ' + tiemposInteresantes(valorFinal1, valorFinal2) + ' tiempos interesantes entre ' + mostrarHora(valorFinal1) + ' y ' + mostrarHora(valorFinal2));


function tiemposInteresantes(t1, t2) {

    acumulador = 0;

    for (let t = t1; t <= t2; t++) {

        acumulador = acumulador + esInteresante(t);
    }

    return acumulador;

}

function esInteresante(t) {

    var formato = ConvertirHora(t);

    var num = ConvertirArreglo(formato);
    console.log(num);

    //Eliminamos valores duplicados del arreglo
    const dataArr = new Set(num);
    let results = [...dataArr];
    console.log(results);


    var cant = results.length;


    //validamos la cantidad del arreglo
    if (cant <= 2) {
        return 1;
    } else {
        return 0;
    }


}


function ConvertirHora(hora) {

    let seg = hora;
    var date = new Date(seg * 1000); //obtenemos la fecha actual
    var hours = date.getHours();
    var minutes = "0" + date.getMinutes();
    var seconds = "0" + date.getSeconds();

    // Mostramos en HH:MM:SS
    var formato = hours + minutes.substr(-2) + seconds.substr(-2);

    return formato;
}

function mostrarHora(hora) {

    let seg = hora;
    var date = new Date(seg * 1000); //obtenemos la fecha actual
    var hours = date.getHours();
    var minutes = "0" + date.getMinutes();
    var seconds = "0" + date.getSeconds();

    // Mostramos en HH:MM:SS
    var formato = hours + ":" + minutes.substr(-2) + ":" + seconds.substr(-2);

    return formato;
}