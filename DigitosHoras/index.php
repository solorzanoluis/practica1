<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="estilo.css">
    
    <title>Tarea 2</title>
</head>
<body>
    
   <div class="contenedor">
        <form action="validar.php" name="form" id="formulario" method="POST">

            <div class="mb-3">
                <label for="" class="form-label">Digita el primer horario</label><br>
                <input class="form-control" name="hora1" id="horario1" type="text" maxlength="8"><p>
            </div>

            <div class="mb-3">
                <label for="" class="form-label">Digita el segundo horario</label><br>
                <input class="form-control" name="hora2" id="horario2" type="text" maxlength="8"><p>
            </div>

            <button class="btn btn-primary" type="submit" name="enviar">Validar</button>
        </form>
   </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
 
    
</body>
</html>