function algoritmo(tarjeta){
  //aqui va toda la programacion del algoritmo
  var sum     = 0,
    alt     = true,
    i       = tarjeta.length-1,//comanzamos de derecha a izquierda
    num;

  while (i >= 0){

      //Se estaran tomando cada caracter numerico enteros ingresado en tarjeta
      num = parseInt(tarjeta.charAt(i), 10);

      //Válida el cambio true o false de imparidad
      if (alt) {
          num *= 2;
          if (num > 9){
            num = (num % 10) + 1;
          }

          if (num == 10){
              num = (num % 10) + 1;
          }
      }
      //Voltea el bit de paridad
      alt = !alt;
      //Agrega el número
      sum += num;
      //Continúa con el siguiente dígito
      i--;

    //console.log(sum);

  }

  //Determina si la tarjeta es válida
  if (sum){

    var ultimoDigitosum = sum.toString().split('').pop();

    //aplicamos la formula
    var resp = 10 - ultimoDigitosum;
    var result = resp.toString().split('').pop();

    
    return alert('El número de tarjeta ingresado fue: ' + tarjeta + "\n\n Su número de verificación es: " + result);

  }

  else{
    return alert('Tarjeta inválida');
  }

}

function respuesta(){

  var numero = document.getElementById('tarjeta').value;

  console.log(numero.length);


  if (numero === null || (isNaN(numero) === true)){
    alert('Número de tarjeta inválido');
    console.log(tarjeta1);
    return;
  }

  if (numero.length < 10 || numero.length > 15){
    alert('El número de tarjeta tiene que tener entre 10 y 15 digitos');
    return;
  }

  algoritmo(numero);

}